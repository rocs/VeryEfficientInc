<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\TeamUser;

use App\goals;

use App\Team;

use Illuminate\Support\Facades\Auth;

use Illuminate\Foundation\Validation\ValidatesRequests;

class GoalController extends Controller
{
  public function __construct()
  {
      $this->middleware('auth');
  }

    public function index($id)
    {

        $teams = Team::all();
        $goals = Goals::where('team_id','=',$id)->get();
        //$rol = TeamUser::where('user_id','=',Auth::user()->id)->get();
        //$rol = $rol['0'];
        $cosas = array($goals, $teams, $id);

        return view('goals/goals', compact('cosas'));

    }

    public function create($id){
        $goals = Goals::where('team_id','=',$id)->get();
        $teams = Team::where('id','=',$id)->get();
        $rol = TeamUser::where('user_id','=',Auth::user()->id)->get();
        $rol = $rol[0];
        $cosas = array($teams, $goals);

        if ($rol['rol'] == 'A' || $rol['rol'] == 'W') {
            return view('goals/registerGoal', compact('cosas'));
        }else{
            return view('errors/403');
        }

    }

  public function store(Request $request){

      $this->validate($request, [
          'shortdescription' => 'required',
          'longdescription' => 'required',
          'status' => 'required',
          'publicstatistics' => 'required',
          'teamid' => 'required',

      ]);

      $goal = new Goals();
      $goal->short_description = $request->input("shortdescription");
      $goal->long_description = $request->input("longdescription");
      $goal->status = $request->input('status');
      $goal->public_statistics = $request->input("publicstatistics");
      $goal->team_id = $request->input('teamid');
      $goal->predecessor_goal = $request->input("predecessorgoal");
        if ($goal->save()){
            //return redirect('/maintenance');
            return redirect('/team')
            ->with('success','Goal created successfully');
        }
  }

  public function edit($id)
  {
      $teams = Team::all();
      $goal = Goals::find($id);
      $goals = Goals::all();
      $cosas = array($teams, $goal, $goals);
      return view('goals.editGoal')->with('cosas', $cosas);
  }

  public function update(Request $request, $id)
  {

    $goal = Goals::find($id);
    $goal->short_description = $request->input("shortdescription");
    $goal->long_description = $request->input("longdescription");
    $goal->status = $request->input('status');
    $goal->public_statistics = $request->input("publicstatistics");
    $goal->team_id = $request->input('teamid');
    $goal->predecessor_goal = $request->input("predecessorgoal");
    $goal->save();
    return redirect('/goals');
  }

  public function destroy($id, $idTeam)
  {
      $goal = Goals::find($id);

      $goal->delete();
      return redirect('/goals/'.$idTeam);
  }
}
