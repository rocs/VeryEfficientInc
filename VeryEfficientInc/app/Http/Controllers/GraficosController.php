<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\goals;
use App\lagMeasures;
use App\leadMeasures;
use App\TeamUser;
use App\lagMeasureLogs;
use App\Usuario;
use App\Team;
use Illuminate\Support\Facades\DB;

class GraficosController extends Controller
{
  public function __construct()
  {
      $this->middleware('auth');
  }
    public function grafico1($id)
    {
      $lags= lagMeasures::where('goal_id', '=', $id)->get();
      $dato = $lags[0];
      //dd($dato);
      $goal = goals::find($id);
      $sum;
      foreach ($lags as $lag) {
          $sum =+ ($lag->weight_value*$lag->persentage);
          //dd($lag->weight_value);
      }
      $sum = $sum/100;
      $res = 100-$sum;
      $datos = array($sum, $res,$goal );
      return view('graficos/meta', compact('datos'));
    }
    public function grafico2($id)
    {
      $lags= lagMeasures::where('goal_id', '=', $id)->get();
      $goal = goals::find($id);
      $datos = array($lags,$goal);
      return view('graficos/lag', compact('datos'));
    }
    public function grafico3($leadid,$id)
    {
      $lead= leadMeasures::find($leadid);
      $goal = lagMeasures::find($lead->lag_measure_id);
      $goal = goals::find($goal->goal_id);
      $goal = $goal->short_description;
      $sum = 0;
      $teamUser = TeamUser::where('team_id','=',$id)->get();
      $logs = lagMeasureLogs::where('lead_measure_id', '=', $leadid)->get();
      foreach ($logs as $log) {
        $sum += $log->amount;
      }
      if ($lead->group_measure == 'Y') {
        $teamUser = count($teamUser);
        $stimate = $lead->stimate_value * $teamUser;
      }else {
        $stimate = $lead->stimate_value;
      }
      $datos = array($lead,$sum, $stimate, $goal);
      //dd($datos);
      return view('graficos/lead', compact('datos'));
    }
    public function grafico4($leadid,$id)
    {
      $lead= leadMeasures::find($leadid);
      $goal = lagMeasures::find($lead->lag_measure_id);
      $goal = goals::find($goal->goal_id);
      $goal = $goal->short_description;
      $sum = 0;
      $teamUser = TeamUser::where('team_id','=',$id)->get();
      $users = array();
      foreach ($teamUser as $team) {
        $usuario = Usuario::find($team->user_id);
        $users[] = $usuario;
      }
      $logs = lagMeasureLogs::where('lead_measure_id', '=', $leadid)->get();
      //dd($logs);
      $amount = array( );
      foreach ($users as $user) {
        foreach ($logs as $log) {
          if ($log->user_id == $user->id) {
            $sum += $log->amount;
          }
        }
          $a = array('name'=>$user->name,'sum'=>$sum);
          $amount[] = $a;
          $sum = 0;
      }
      //dd($amount);
      $stimate = $lead->stimate_value;
      $datos = array($lead,$stimate, $goal,$amount);
      //dd($datos);
      return view('graficos/leadIndi', compact('datos'));
    }
    public function grafico5($id)
    {
      $lags = lagMeasures::where('goal_id', '=', $id)->get();
      $teamUser = TeamUser::where('team_id','=',$id)->get();
      $leads = array( );
      foreach ($lags as $lag) {
          $ls = leadMeasures::where('lag_measure_id', '=', $lag->id)->get();
          foreach ($ls as $lead) {
            $leads[]=($lead);
          }
      }
      $goal = goals::find($id);
      $goal = $goal->short_description;
      $amount = array( );
      $sum = 0;
      foreach ($leads as $lead) {
          $logs = lagMeasureLogs::where('lead_measure_id', '=', $lead->id)->get();
          foreach ($logs as  $log) {
            $sum += $log->amount;
          }
          $estimado = $lead->stimate_value * count($teamUser);
          $a = array('name' => $lead->short_description,'amount'=>$sum, 'estimado' => $estimado );
          $amount[] = $a;
          $sum = 0;
      }
      //dd($amount);
      $datos = array($amount, $goal);
      //dd($datos);
      return view('graficos/logro', compact('datos'));
    }
    public function grafico6($leadid)
    {
      $lead= leadMeasures::find($leadid);
      $team = lagMeasures::find($lead->lag_measure_id);
      $team = goals::find($team->goal_id);
      $team = Team::find($team->team_id);
      $teamUser = TeamUser::where('team_id', '=', $team->id)->get();
      $logs = lagMeasureLogs::where('lead_measure_id','=',$leadid)->get();
      $f = 0;
      $datos = array( );
      $paraF = array( );
      foreach ($logs as $log) {
        $fecha = strtotime($log->log_time);
        $fecha = date('Y,m,d',$fecha);
        $a = array('fecha' => $fecha, 'amount' =>0);

          //dd('hh');
        $fechas = lagMeasureLogs::where('log_time', '=', $log->log_time)->get();
        //dd($fechas);
        foreach ($a as $b) {

          foreach ($fechas as $y) {
            $est = true;
              if (date('Y,m,d',strtotime($y->log_time)) == $b) {

                foreach ($datos as $dato) {
                  if ($dato['fecha'] == date('Y,m,d',strtotime($y->log_time))) {
                    $est = false;
                  }
                }
                if ($est) {
                  $f += $y->amount;
                }
              }

          }
        }
        if ($est) {
          //$paraF[] = ($f);
          $a['amount'] = ($f);
        }
        //dd($a);
        if (count($datos)>0) {
          foreach ($datos as $dato) {
            $est = true;
            //dd($a);
            if ($a['amount'] == 0) {
              $est = false;
            }
          }
        }
        if ($est) {
          $datos[] = ($a);
        }
        $f=0;
      }
      //dd($datos);
      $cosas = array($lead->short_description,$datos,$lead->stimate_value*count($teamUser));
      //dd($cosas);
        return view('graficos/fecha', compact('cosas'));
    }
}
