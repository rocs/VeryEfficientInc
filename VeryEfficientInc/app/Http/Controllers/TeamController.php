<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Team;
use Auth;
use App\TeamUser;

class TeamController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index (){
      $Teams = Team::all();
        if (Auth::user()->rol == 1) {
            return view('team/index', compact('Teams'));
        }else{
            return view('errors/403');
        }
    }

    public function create(){
      return view('team/register', compact('Teams'));
    }

    public function store(Request $request){
      $this->validate($request, [
          'team_id' => 'required',
          'description' => 'required',
      ]);

      $team = new Team();
      $team->team_id = $request->input("team_id");
      $team->description = $request->input("description");
      if ($team->save()){
        $id = $team->orderby('created_at','DESC')->take(1)->get();
        $id = $id[0];
        $idUser = Auth::user()->id;
        $TeamUser = new TeamUser;
        $TeamUser->team_id = $id->id;
        $TeamUser->user_id = $idUser;
        $TeamUser->rol = 'A';
          if($TeamUser->save()){
            return redirect('/team');
          }
      }

    }

    public function edit($id)
    {
      $team = Team::find($id);
      return view('team/update')->with('team', $team);
    }

    public function update(Request $request, $id)
    {
      $team = Team::find($id);
      $team->team_id = $request->get('team_id');
      $team->description = $request->input("description");
      $team->save();
      return redirect('/team');
    }

    public function destroy($id)
    {
        $team = Team::find($id);

        $team->delete();
        return redirect('/team');
    }

}
