<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests;
use App\Team;
use App\TeamUser;
use App\Usuario;
use Auth;
class TeamUserController extends Controller
{
  public function __construct()
  {
      $this->middleware('auth');
  }
    public function index(){
      $datos= DB::table('team_user')
            ->join('teams', 'team_user.team_id', '=', 'teams.id')
            ->join('users', 'team_user.user_id', '=', 'users.id')
            ->select('team_user.id', 'teams.team_id', 'users.name','team_user.rol')
            ->get();
        return view('teamUser/index', ['items' => $datos]);
    }
    public function create(){
      if ($this->validar()) {
        $teams = new Team;
        $teams = $teams->Traer();
        $usuarios = new Usuario;
        $usuarios = $usuarios->Traer();
        $datos = ['teams'=>$teams,'usuarios'=>$usuarios];
        return view ('teamUser.register',$datos);
      }
      else {
        return view('errors/403');
      }

    }
    public function store(Request $request){
      $this->validate($request, [
          'team_id' => 'required',
          'user_id' => 'required',
          'rol'=>'required'
      ]);
      $TeamUser = new TeamUser;
      $TeamUser->team_id = $request->input("team_id");
      $TeamUser->user_id = $request->input("user_id");
      $TeamUser->rol = $request->input("rol");
      if ($TeamUser->save()){
          return redirect('teamUser');
      }

    }
    public function nuevo(Request $request){
      $TeamUser = new TeamUser;
      $TeamUser->team_id = $request->input("team_id");
      $TeamUser->user_id = $request->input("user_id");
      $TeamUser->rol =  'R';
      if ($TeamUser->save()){
        return $this->especial($request->input("idTeam"),$request->input("teamName"));
      }
    }

    public function edit($id)
    {
      $teamUser= teamUser::find($id);
      $nameUser = Usuario::find($teamUser->user_id);
      $teams = Team::all();
      $datos = array($teamUser,$nameUser,$teams);
      //dd($datos);
      return view('teamUser/update',compact('datos'));
    }

    public function update(Request $request, $id)
    {
      $this->validate($request, [
          'team_id' => 'required',
          'user_id' => 'required',
          'rol'=>'required'
      ]);
      $team = TeamUser::find($id);
      $team->team_id = $request->get('team_id');
      $team->rol = $request->input("rol");
      $team->save();
      return redirect('/teamUser');
    }

    public function destroy($id)
    {
        $teamUser = TeamUser::find($id);
        $teamUser->delete();
        return redirect('/teamUser');
    }
    public function destroy1($id,$teamName, $idTeam){
      $teamUser = TeamUser::find($id);
      $teamUser->delete();
      return $this->especial($idTeam,$teamName);
    }
    public function especial($id, $teamName){
      $teamUsers = TeamUser::where('team_id','=', $id)->get();
      $usuarios = Usuario::all();
      $datos = array($teamUsers,$usuarios,$teamName,$id);
      //dd($datos);
      return view('/teamUser/membresia', compact('datos'));
    }
    public function cambio($id, $teamName, $idTeam){
        $teamUser = new teamUser;
        $teamUser = $teamUser::find($id);
        //dd($teamUser);
        if ($teamUser->rol === 'A') {
          $teamUser->rol = 'R';
        }else {
          $teamUser->rol = 'A';
        }
        if ($teamUser->save()) {
          return $this->especial($idTeam,$teamName);
        }

    }
    public function validar(){
      if (Auth::user()->rol == 1) {
        return true;
      }
      else {
        return false;
      }
    }

}
