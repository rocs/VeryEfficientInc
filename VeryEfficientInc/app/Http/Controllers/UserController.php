<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Usuario;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Validation\ValidatesRequests;

class UserController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $Usuario = Usuario::all();
        if (Auth::user()->rol == 1) {
            return view('mantenimiento/maintenance')->with('Usuario',$Usuario);
        }else{
            return view('errors/403');
        }


    }

    public function create(){
        return view('/register')->with('Usuario',$Usuario);
    }

    public function store(Request $request){

        $this->validate($request, [
            'name' => 'required',
            'email' => 'required',
            'password' => 'required',
            'rol' => 'required',
        ]);

        $usuario = new Usuario();
        $usuario->name = $request->input("name");
        $usuario->email = $request->input("email");
        $usuario->password = bcrypt($request->input('password'));
        $usuario->rol = $request->input("rol");
        $usuario->remember_token = str_random(59);


          if ($usuario->save()){
              //return redirect('/maintenance');
              return redirect('/maintenance')
              ->with('success','Usuario created successfully');
          }




    }

    public function edit($id)
    {
        $usuario = Usuario::find($id);
        return view('mantenimiento.edit')->with('usuario', $usuario);
    }

    public function update(Request $request, $id)
    {
      $usuario = Usuario::find($id);
      $usuario->name = $request->get('name');
      $usuario->email = $request->input("email");
      $usuario->rol = $request->input("rol");
      $usuario->save();
      return redirect('/maintenance');
    }

    public function destroy($id)
    {
        $usuario = Usuario::find($id);

        $usuario->delete();
        return redirect('/maintenance');
    }
}
