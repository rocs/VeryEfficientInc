<?php
namespace App\Http\Controllers;
use App\LagMeasureLogs;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\TeamUser;
use App\goals;
use App\team;
use App\lagMeasures;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Validation\ValidatesRequests;
class lagMeasureController extends Controller
{
  public function __construct()
  {
      $this->middleware('auth');
  }
    public function index($id)
    {
        $lagMeasures = LagMeasures::where('goal_id','=',$id)->get();
        $goals = Goals::all();
        $cosas = array($goals, $lagMeasures, $id);
        return view('lag/index', compact('cosas'));
    }
    public function create($id){
        $goals = Goals::where('id','=',$id)->get();
        $rol = TeamUser::where('user_id','=',Auth::user()->id)->get();
        $rol = $rol[0];
        if ($rol['rol'] === 'A' || $rol['rol'] === 'W') {
            return view('lag/register', compact('goals'));
        }else{
            return view('errors/403');
        }
    }
  public function store(Request $request){
      $this->validate($request, [
          'shortdescription' => 'required',
          'longdescription' => 'required',
          'weight_value' => 'required',
          'goal_id' => 'required',
          'persentage' => 'required',
      ]);
      $lag = new LagMeasures();
      $lag->short_description = $request->input("shortdescription");
      $lag->long_description = $request->input("longdescription");
      $lag->weight_value = $request->input('weight_value');
      $lag->persentage = $request->input("persentage");
      $lag->goal_id = $request->input('goal_id');
        if ($lag->save()){
            //return redirect('/maintenance');
            return redirect('/goals/'.$request->input('goal_id'))
            ->with('success','Goal created successfully');
        }
  }
  public function edit($id, $id_goal)
  {
      $teams = Team::all();
      $lag = LagMeasures::find($id);
      $goals = Goals::find($id_goal);
      $cosas = array($lag, $goals);
      return view('lag.edit')->with('cosas', $cosas);
  }
  public function update(Request $request, $id)
  {
      $this->validate($request, [
          'shortdescription' => 'required',
          'longdescription' => 'required',
          'weight_value' => 'required',
          'goal_id' => 'required',
          'persentage' => 'required',
      ]);
    $lag = LagMeasures::find($id);
      $lag->short_description = $request->input("shortdescription");
      $lag->long_description = $request->input("longdescription");
      $lag->weight_value = $request->input('weight_value');
      $lag->persentage = $request->input("persentage");
      $lag->goal_id = $request->input('goal_id');
      $lag->save();
    return redirect('/team');
  }
  public function destroy($id)
  {
      $lag = LagMeasures::find($id);
      $lag->delete();
      return redirect('/team');
  }
}
