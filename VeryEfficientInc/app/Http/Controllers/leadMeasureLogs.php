<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\lagMeasureLogs;
use App\leadMeasures;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
class leadMeasureLogs extends Controller
{
  public function __construct()
  {
      $this->middleware('auth');
  }
  public function index(){
    $datos= DB::table('lead_measures_logs')
          ->join('lead_measures', 'lead_measures_logs.lead_measure_id', '=', 'lead_measures.id')
          ->join('users', 'lead_measures_logs.user_id', '=', 'users.id')
          ->select('lead_measures_logs.id', 'lead_measures.short_description', 'users.name','lead_measures_logs.amount','lead_measures_logs.log_time')
          ->get();
          //dd($datos);
      return view('log/index', ['items' => $datos]);
  }
  public function create($id){
    $usuario = Auth::user();
    $leadmeasures = leadMeasures::find($id);
    $datos = array($usuario ,$leadmeasures );
    //dd($datos);
    return view('log/create ', compact('datos'));
  }

  public function store(Request $request){
      $this->validate($request, [
          'user_id' => 'required',
          'lead_id' => 'required',
          'amount' => 'required',
          'log_time' => 'required',
      ]);
      $log = new lagMeasureLogs();
      $log->lead_measure_id = $request->input("lead_id");
      $log->user_id = $request->input("user_id");
      $log->amount = $request->input('amount');
      $log->log_time = $request->input("log_time");
        if ($log->save()){
            return redirect('/log');
        }
      }
      public function edit($id){
        $usuario = Auth::user();
        $log = lagMeasureLogs::find($id);
        //dd($log);
        $leadmeasures = leadMeasures::find($log->lead_measure_id);
        $datos = array($usuario ,$leadmeasures,$log);
        //dd($datos);
        return view('log/update')->with('datos', $datos);
      }
      public function update(Request $request, $id){
          $this->validate($request, [
              'user_id' => 'required',
              'lead_id' => 'required',
              'amount' => 'required',
              'log_time' => 'required',
          ]);
          $log = lagMeasureLogs::find($id);
          $log->lead_measure_id = $request->input("lead_id");
          $log->user_id = $request->input("user_id");
          $log->amount = $request->input('amount');
          $log->log_time = $request->input("log_time");
            if ($log->save()){
                return redirect('/log');
            }
          }
          public function destroy($id)
          {
              $lead = lagMeasureLogs::find($id);

              $lead->delete();
              return redirect('/log');
          }




}
