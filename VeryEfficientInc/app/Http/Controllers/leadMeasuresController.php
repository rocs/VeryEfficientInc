<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\TeamUser;
use App\goals;
use App\Team;
use App\lagMeasures;
use App\leadMeasures;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Validation\ValidatesRequests;

class leadMeasuresController extends Controller
{
  public function __construct()
  {
      $this->middleware('auth');
  }

  public function index($id, $idGrup)
  {


    $lag = LagMeasures::where('id','=',$id)->get();
    $lead = LeadMeasures::where('lag_measure_id','=',$id)->get();

    $grup = $idGrup;
    $cosas = array($lead, $lag, $grup);
    //dd($cosas[1]);
          return view('leadMeasures/index', compact('cosas'));

  }

  public function create($id, $idGrup){

    $lag = LagMeasures::where('id','=',$id)->get();

    $rol = TeamUser::where('user_id','=',Auth::user()->id)->get();
    $rol = $rol[0];
    $cosas = array($lag , $idGrup);

    if ($rol['rol'] === 'A') {
        return view('leadMeasures/register')->with('cosas',$cosas);
    }else{
        return view('errors/403');
    }


  }

  public function store(Request $request, $id){
      $this->validate($request, [
          'shortdescription' => 'required',
          'longdescription' => 'required',
          'lag' => 'required',
          'stimate_value' => 'required',
          'periodicity_days' => 'required',
          'group_measure' => 'required',
          'start_date' => 'required',
          'estatus' => 'required',
      ]);

      $lead = new LeadMeasures();
      $lead->short_description = $request->input("shortdescription");
      $lead->long_description = $request->input("longdescription");
      $lead->lag_measure_id = $request->input('lag');
      $lead->stimate_value = $request->input("stimate_value");
      $lead->periodicity_days = $request->input("periodicity_days");
      $lead->group_measure = $request->input("group_measure");
      $lead->start_date = $request->input("start_date");
      $lead->estatus = $request->input("estatus");
        if ($lead->save()){
            //return redirect('/maintenance');
            return redirect('lag/'.$id)
            ->with('success','Usuario created successfully');
        }
  }

  public function edit($id, $idLag)
  {
      $lag = LagMeasures::find($idLag);
      $lead = LeadMeasures::find($id);
        $cosas = array($lag, $lead);

      return view('leadMeasures.edit')->with('cosas', $cosas);
  }

  public function update(Request $request, $id)
  {
    $this->validate($request, [
        'shortdescription' => 'required',
        'longdescription' => 'required',
        'lag' => 'required',
        'stimate_value' => 'required',
        'periodicity_days' => 'required',
        'group_measure' => 'required',
        'start_date' => 'required',
        'estatus' => 'required',
    ]);


    $leadmeasures = LeadMeasures::find($id);
    $leadmeasures->short_description = $request->input("shortdescription");
    $leadmeasures->long_description = $request->input("longdescription");
    $leadmeasures->lag_measure_id = $request->input('lag');
    $leadmeasures->stimate_value = $request->input("stimate_value");
    $leadmeasures->periodicity_days = $request->input("periodicity_days");
    $leadmeasures->group_measure = $request->input("group_measure");
    $leadmeasures->start_date = $request->input("start_date");
    $leadmeasures->estatus = $request->input("estatus");
    if ($leadmeasures->save()){
        //return redirect('/maintenance');
        return redirect('team/');
    }
  }

  public function destroy($id)
  {
      $lead = LeadMeasures::find($id);

      $lead->delete();
      return redirect('/team');
  }
}
