<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Team extends Model
{
  protected $table = "teams";

  protected $fillable = [
      'id','team_id', 'description',
  ];
  public function Traer()
  {
    $teams = DB::table('teams')->get();
    return $teams;
  }

}
