<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class teamUser extends Model
{
    //
    protected $table = "team_user";

    protected $fillable = [
        'team_id', 'user_id','rol',
    ];
    public function join(){
    return $datos=  DB::table('team_user')
            ->join('teams', 'team_user.team_id', '=', 'teams.id')
            ->join('users', 'team_user.user_id', '=', 'users.id')
            ->select('team_user.id','teams.id as teamID', 'teams.team_id','users.id as userID' ,'users.name','team_user.rol')
            ->where('team_user.id','=', $id)
            ->first();
    }
}
