<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class usuario extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = "users";

    protected $fillable = [
        'name', 'email', 'password','rol',
    ];
    public function Traer()
    {
      $usuarios = DB::table('users')->get();
      return $usuarios;
    }



}
