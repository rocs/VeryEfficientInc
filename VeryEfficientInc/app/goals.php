<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class goals extends Model
{

  protected $table = "goals";

  protected $fillable = [
      'short_description', 'long_description', 'status','public_statistics','team_id','predecessor_goal',
  ];

}
