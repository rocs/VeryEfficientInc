<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LagMeasureLogs extends Model
{
  protected $table = "lead_measures_logs";

  protected $fillable = [
      'lead_measure_id', 'user_id', 'amount','log_time',
  ];
}
