<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LagMeasures extends Model
{
  protected $table = "lag_measures";

  protected $fillable = [
      'short_description', 'long_description', 'weight_value','	persentage','goal_id',
  ];
}
