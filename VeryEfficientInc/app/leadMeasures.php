<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LeadMeasures extends Model
{
  protected $table = "lead_measures";

  protected $fillable = [
      'short_description', 'long_description', 'lag_measure_id','	stimate_value','periodicity_days','group_measure','start_date','estatus',
  ];
}
