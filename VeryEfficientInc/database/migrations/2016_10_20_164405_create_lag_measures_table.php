<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLagMeasuresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lag_measures', function (Blueprint $table) {
            $table->increments('id');
            $table->string('short_description');
            $table->text('long_description');
            $table->integer('weight_value');
            $table->integer('persentage');
            $table->integer('goal_id')->references('id')->on('goals');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('lag_measures');
    }
}
