<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeadMeasureTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lead_measures', function (Blueprint $table) {
            $table->increments('id');
            $table->string('short_description');
            $table->text('long_description');
            $table->integer('lag_measure_id')->references('id')->on('lag_measures');
            $table->integer('stimate_value');
            $table->integer('periodicity_days');
            $table->enum('group_measure', ['Y','N']);
            $table->date('start_date');
            $table->enum('estatus', ['E','D']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('lead_measures');
    }
}
