<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeadMeasuresLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
         Schema::create('lead_measures_logs', function (Blueprint $table) {
             $table->increments('id');
             $table->integer('lead_measure_id')->references('id')->on('lead_measures');
             $table->integer('user_id')->references('id')->on('users');
             $table->integer('amount');
             $table->timestamp('log_time');
             $table->timestamps();
         });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         Schema::drop('lead_measures_logs');
     }
}
