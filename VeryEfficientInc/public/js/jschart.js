window.onload = function () {
    var chart = new CanvasJS.Chart("quezt1",
        {
            theme: "theme3",
            title:{
                text: "¿Es usted educador o estudiante de un centro educativo?"
            },
            data: [
                {
                    type: "pie",
                    showInLegend: true,
                    toolTipContent: "{y} - #percent %",
                    yValueFormatString: "#,###,###,###.##",
                    legendText: "{indexLabel}",
                    dataPoints: [
                        {  y: 36, indexLabel: "Educador" },
                        {  y: 18, indexLabel: "Estudiante" },
                    ]
                }
            ]
        });
    chart.render();

    var chart = new CanvasJS.Chart("quezt2",
        {
            theme: "theme3",
            title:{
                text: "¿Cree usted que sería eficiente que se le haga conocer al estudiante sus notas durante el curso lectivo?(Solo Educadores)"
            },
            data: [
                {
                    type: "pie",
                    showInLegend: true,
                    toolTipContent: "{y} - #percent %",
                    yValueFormatString: "#,###,###,###.##",
                    legendText: "{indexLabel}",
                    dataPoints: [
                        {  y: 35, indexLabel: "Sí" },
                        {  y: 1, indexLabel: "No" },
                    ]
                }
            ]
        });
    chart.render();

    var chart = new CanvasJS.Chart("quezt3",
        {
            theme: "theme3",
            title:{
                text: "¿En comparación con el sistema actual de control de notas, le gustaría un sistema más facilidad de uso?(Solo Educadores)"
            },
            data: [
                {
                    type: "pie",
                    showInLegend: true,
                    toolTipContent: "{y} - #percent %",
                    yValueFormatString: "#,###,###,###.##",
                    legendText: "{indexLabel}",
                    dataPoints: [
                        {  y: 35, indexLabel: "Sí" },
                        {  y: 1, indexLabel: "No" },
                    ]
                }
            ]
        });
    chart.render();

    var chart = new CanvasJS.Chart("quezt4",
        {
            theme: "theme3",
            title:{
                text: "¿Le gustaría que la aplicación de calificación actualice las notas con solo tener acceso a Internet?(Solo Educadores)"
            },
            data: [
                {
                    type: "pie",
                    showInLegend: true,
                    toolTipContent: "{y} - #percent %",
                    yValueFormatString: "#,###,###,###.##",
                    legendText: "{indexLabel}",
                    dataPoints: [
                        {  y: 35, indexLabel: "Sí" },
                        {  y: 1, indexLabel: "No" },
                    ]
                }
            ]
        });
    chart.render();

    var chart = new CanvasJS.Chart("quezt5",
        {
            theme: "theme3",
            title:{
                text: "¿Le gustaría una aplicación que le muestre un registros de sus notas en el transcurso del curso lectivo?(Solo Estudiantes)"
            },
            data: [
                {
                    type: "pie",
                    showInLegend: true,
                    toolTipContent: "{y} - #percent %",
                    yValueFormatString: "#,###,###,###.##",
                    legendText: "{indexLabel}",
                    dataPoints: [
                        {  y: 17, indexLabel: "Sí" },
                        {  y: 1, indexLabel: "No" },
                    ]
                }
            ]
        });
    chart.render();

    var chart = new CanvasJS.Chart("quezt6",
        {
            theme: "theme3",
            title:{
                text: "¿Le gustaría poder poner recordatorios de exámenes o trabajos?(Solo Estudiantes"
            },
            data: [
                {
                    type: "pie",
                    showInLegend: true,
                    toolTipContent: "{y} - #percent %",
                    yValueFormatString: "#,###,###,###.##",
                    legendText: "{indexLabel}",
                    dataPoints: [
                        {  y: 18, indexLabel: "Sí" },
                        {  y: 0, indexLabel: "No" },
                    ]
                }
            ]
        });
    chart.render();

    var chart = new CanvasJS.Chart("quezt7",
        {
            theme: "theme3",
            title:{
                text: "¿Le gustaría que el sistema de calificación se pueda acceder desde el celular?"
            },
            data: [
                {
                    type: "pie",
                    showInLegend: true,
                    toolTipContent: "{y} - #percent %",
                    yValueFormatString: "#,###,###,###.##",
                    legendText: "{indexLabel}",
                    dataPoints: [
                        {  y: 53, indexLabel: "Sí" },
                        {  y: 1, indexLabel: "No" },
                    ]
                }
            ]
        });
    chart.render();

    var chart = new CanvasJS.Chart("quezt8",
        {
            theme: "theme3",
            title:{
                text: "¿Cree usted que sería útil un espacio para comentarios del profesor al estudiante?"
            },
            data: [
                {
                    type: "pie",
                    showInLegend: true,
                    toolTipContent: "{y} - #percent %",
                    yValueFormatString: "#,###,###,###.##",
                    legendText: "{indexLabel}",
                    dataPoints: [
                        {  y: 50, indexLabel: "Sí" },
                        {  y: 4, indexLabel: "No" },
                    ]
                }
            ]
        });
    chart.render();

    var chart = new CanvasJS.Chart("quezt9",
        {
            theme: "theme3",
            title:{
                text: "¿Le gustaría que la aplicación le notifique cuando el periodo lectivo este cerca de finalizar?"
            },
            data: [
                {
                    type: "pie",
                    showInLegend: true,
                    toolTipContent: "{y} - #percent %",
                    yValueFormatString: "#,###,###,###.##",
                    legendText: "{indexLabel}",
                    dataPoints: [
                        {  y: 54, indexLabel: "Sí" },
                        {  y: 0, indexLabel: "No" },
                    ]
                }
            ]
        });
    chart.render();
}