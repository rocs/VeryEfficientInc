@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Editar Goal</div>
                    <div class="panel-body">
                      <?php $teams = $cosas[0] ?>
                      <?php $meta = $cosas[1] ?>
                      <?php $goals = $cosas[2] ?>
                      <?php $goal = $meta['id'] ?>
                      <?php $short = $meta['short_description'] ?>
                      <?php $long = $meta['long_description'] ?>


                        <form class="form-horizontal" role="form" method="POST" action="{{ url('goals/edit', $goal) }}">
                            {{ csrf_field() }}
                            <input type="hidden" name="_method" value="PATCH">
                            <!-- Form Input descricion corta-->
                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                    <label for="name" class="col-md-4 control-label">Short Description</label>

                                    <div class="col-md-6">
                                        <input id="shortdescription" type="text" class="form-control" name="shortdescription" value="{{ $short }}" required autofocus>

                                        @if ($errors->has('shortdescription'))
                                            <span class="help-block">
                                            <strong>{{ $errors->first('shortdescription') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>

                            <!-- Form Input descricion larga-->
                                <div class="form-group{{ $errors->has('longdescription') ? ' has-error' : '' }}">
                                    <label for="email" class="col-md-4 control-label">Long Description</label>

                                    <div class="col-md-6">
                                        <textarea id="longdescription" name="longdescription" type="text" class="form-control" value="{{ $long }}" required autofocus>{{ $long }}</textarea>
                                        @if ($errors->has('longdescription'))
                                            <span class="help-block">
                                            <strong>{{ $errors->first('longdescription') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>

                                <!-- Form Input Estado-->
                                    <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                                        <label for="status" class="col-md-4 control-label">Estados</label>

                                        <div class="col-md-6">
                                            <select name="status" id="status">
                                                @if($meta->status === 'N')
                                                    <option value="N">Null</option>
                                                    <option value="C">Completdo</option>
                                                    <option value="P">Pendiente</option>
                                                @endif
                                                @if($meta->status === 'C')
                                                <option value="C">Completdo</option>
                                                    <option value="N">Null</option>
                                                    <option value="P">Pendiente</option>
                                                @endif
                                                @if($meta->status === 'P')
                                                <option value="P">Pendiente</option>
                                                    <option value="N">Null</option>
                                                    <option value="C">Completdo</option>
                                                @endif
                                            </select>
                                        </div>

                                        @if ($errors->has('status'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('status') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                            <!-- Form Input equipo-->
                            <div class="form-group{{ $errors->has('teamid') ? ' has-error' : '' }}">
                                <label for="team_id" class="col-md-4 control-label">Equipo</label>

                                <div class="col-md-6">
                                    <select name="teamid" id="teamid">
                                      <?php foreach ($teams as $team): ?>
                                        <?php if ($team->id === $meta->team_id): ?>
                                          <option value= @$team['id'] ><?php echo $team['team_id'];?></option>
                                        <?php endif; ?>
                                      <?php endforeach; ?>
                                      <?php foreach ($teams as $team):
                                      echo '<option value='.$team['id'].'>'.$team['team_id'].'</option>';
                                    endforeach; ?>

                                    </select>
                                </div>

                                    @if ($errors->has('teamid'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('teamid') }}</strong>
                                    </span>
                                    @endif
                            </div>

                            <div class="form-group{{ $errors->has('publicstatistics') ? ' has-error' : '' }}">
                                <label for="public_statistics" class="col-md-4 control-label">Estadisticas</label>

                                <div class="col-md-6">
                                    <select name="publicstatistics" id="publicstatistics">

                                      <?php if ($meta['public_statistics'] === 'N'): ?>
                                        <option value="N">Privadas</option>
                                        <option value="Y">Publicas</option>
                                      <?php endif; ?>
                                      <?php if ($meta['public_statistics'] === 'Y'): ?>
                                        <option value="Y">Publicas</option>
                                        <option value="N">Privadas</option>
                                      <?php endif; ?>
                                    </select>
                                </div>

                                    @if ($errors->has('publicstatistics'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('publicstatistics') }}</strong>
                                    </span>
                                    @endif
                            </div>

                            <div class="form-group{{ $errors->has('predecessorgoal') ? ' has-error' : '' }}">
                                <label for="predecessorgoal" class="col-md-4 control-label">Meta Predesesora</label>

                                <div class="col-md-6">
                                  <?php $metas= $cosas[0];?>
                                    <select name="predecessorgoal" id="predecessorgoal">

                                      <?php if ($meta->predecessor_goal > 0): ?>
                                          <?php foreach ($metas as $metas): ?>
                                            <?php if ($meta->predecessor_goal === $metas->id): ?>
                                              <td>
                                                {{$goals->short_description}}
                                                <option value="{{$goals->id}}">{{$goals->short_description}}</option>
                                              </td>
                                            <?php endif; ?>
                                          <?php endforeach; ?>
                                      <?php endif; ?>
                                      <?php if ($meta->predecessor_goal === 0): ?>
                                        <option value="">Ninguna</option>
                                      <?php endif; ?>
                                      <?php foreach ($goals as $goals):
                                      echo '<option value='.$goals['id'].'>'.$goals['short_description'].'</option>';
                                    endforeach; ?>

                                    </select>
                                </div>

                                    @if ($errors->has('predecessorgoal'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('predecessorgoal') }}</strong>
                                    </span>
                                    @endif
                            </div>

                            <!-- Form Input Button-->
                                <div class="form-group">
                                    <div class="col-md-6 col-md-offset-4">
                                        <button type="submit" class="btn btn-primary">
                                            Actualizar
                                        </button>
                                    </div>
                                </div>
                            </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
