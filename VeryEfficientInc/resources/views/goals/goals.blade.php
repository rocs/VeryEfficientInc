@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 ">
            <div class="panel panel-default">
                <div class="panel-heading">Metas</div>

                <div class="panel-body">

                  <a href="{{ url('/team') }}"><button type="button" class="btn btn-success" name="button">Atras</button></a>
                  <br>
                  <?php
                   $goals = $cosas[0];
                   $teams = $cosas[1];
                   $id = $cosas[2];
                  ?>

                  @if(count($goals)>0)
                  <div class="table-responsive">
                  <table class="table table-bordered table-hover">
                      <thead>
                        <tr>

                          <td>
                            Short Description
                          </td>
                          <td>
                            Long Description
                          </td>
                          <td>
                            Estatus
                          </td>
                          <td>
                            Team
                          </td>
                          <td>
                            Predecessor goal
                          </td>
                          <td>
                            Lead Measure
                          </td>
                          <td>
                            Graficos
                          </td>
                          <td>
                            Acción
                          </td>
                        </tr>
                      </thead>
                      @foreach ($goals as $goals)
                      <tbody>
                        <td>{{$goals->short_description}}</td>
                        <td>{{$goals->long_description}}</td>
                        <?php if ($goals->status == 'C'): ?>
                          <td>
                            Completada
                          </td>
                        <?php endif; ?>
                        <?php if ($goals->status == 'P'): ?>
                          <td>
                            Pendiente
                          </td>
                        <?php endif; ?>
                        <?php if ($goals->status == 'N'): ?>
                          <td>
                            Nula
                          </td>
                        <?php endif; ?>
                        <?php foreach ($teams as $team): ?>

                          <?php if ($team->id == $goals->team_id): ?>
                            <td>
                              {{$team->team_id}}
                            </td>
                          <?php endif; ?>
                        <?php endforeach; ?>
                        <?php $metas= $cosas[0];?>

                        <?php if ($goals->predecessor_goal > 0): ?>
                            <?php foreach ($metas as $metas): ?>
                              <?php if ($goals->predecessor_goal == $metas->id): ?>
                                <td>
                                  {{$metas->short_description}}
                                </td>
                              <?php endif; ?>
                            <?php endforeach; ?>
                        <?php endif; ?>
                        <?php if ($goals->predecessor_goal == 0): ?>
                          <td>
                            No hay
                          </td>
                        <?php endif; ?>
                        <td>
                          <a class="btn btn-primary" href="/lag/{!! $goals->id !!}">Ver Lag Measures</a>
                          <a class="btn btn-primary" href="/lag/add/{!! $goals->id !!}">Anadir Lag Measures</a>
                        </td>
                        <td>
                          <a class="btn btn-primary" href="/grafico/meta/{!! $goals->id !!}">Ver Completado vs Pendiente</a>
                            <a class="btn btn-primary" href="/grafico/lag/{!! $goals->id !!}">Ver Avance de medida ponderable</a>
                            <a class="btn btn-primary" href="/grafico/logro/{!! $goals->id !!}">Ver Logros vs Estimado</a>
                        </td>
                        <td>
                          <a class="btn btn-primary" href="/goal/edit/{!! $goals->id !!}">Editar</a>
                          <form action="{{ url('goal/destroy/'.$goals->id.'/'.$id) }}" method="post">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="_method" value="DELETE">
                            <button type="submit" class="btn btn-danger">
                              <span  aria-hidden="true">Eliminar</span>
                            </button>
                          </form>
                        </td>
                      </tbody>
                       @endforeach
                    </table>
                      @else
                          <h2>No hay Goals</h2>
                      @endif
                </div>
              </div>
            </div>
        </div>
    </div>
</div>
@endsection
