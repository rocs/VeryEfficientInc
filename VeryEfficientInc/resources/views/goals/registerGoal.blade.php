@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Register</div>
                    <div class="panel-body">
                      <?php $teams = $cosas[0] ?>
                        <?php $teams = $teams[0] ?>
                      <?php $goals = $cosas[1] ?>
                        <form class="form-horizontal" role="form" method="POST" action="/goals/create">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('shortdescription') ? ' has-error' : '' }}">
                                <label for="shor_description" class="col-md-4 control-label">Descripcion Corta</label>

                                <div class="col-md-6">
                                    <input id="shortdescription" type="text" class="form-control" name="shortdescription" value="{{ old('shortdescription') }}" required autofocus>

                                    @if ($errors->has('shortdescription'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('shortdescription') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('longdescription') ? ' has-error' : '' }}">
                                <label for="longdescription" class="col-md-4 control-label">Descripcion Detallada</label>

                                <div class="col-md-6">
                                    <textarea rows="4" cols="50" id="longdescription" type="text" class="form-control" name="longdescription" value="{{ old('longdescription') }}" required autofocus></textarea>

                                    @if ($errors->has('longdescription'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('longdescription') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('teamid') ? ' has-error' : '' }}">
                                <label for="team_id" class="col-md-4 control-label">Equipo</label>

                                <div class="col-md-6">
                                    <input readonly type="text" class="form-control" value="{{$teams['team_id']}}" required autofocus>
                                    <textarea readonly rows="1" cols="2" id="teamid" type="text" class="form-control" name="teamid" value="{{$teams['id']}}" style="visibility:hidden" required autofocus>{{$teams['id']}}</textarea>

                                </div>

                                    @if ($errors->has('teamid'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('teamid') }}</strong>
                                    </span>
                                    @endif
                            </div>

                            <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                                <label for="status" class="col-md-4 control-label">Estado</label>

                                <div class="col-md-6">
                                    <select name="status" id="status">
                                      <option value="P">Pendiente</option>
                                      <option value="C">Completa</option>
                                        <option value="N">Nula</option>
                                    </select>
                                </div>

                                    @if ($errors->has('status'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('status') }}</strong>
                                    </span>
                                    @endif
                            </div>

                            <div class="form-group{{ $errors->has('predecessorgoal') ? ' has-error' : '' }}">
                                <label for="predecessorgoal" class="col-md-4 control-label">Meta Predesesora</label>

                                <div class="col-md-6">
                                    <select name="predecessorgoal" id="predecessorgoal">
                                      <?php foreach ($goals as $goal):
                                      echo '<option value='.$goal['id'].'>'.$goal['short_description'].'</option>';
                                    endforeach; ?>
                                    <option value="">Ninguna</option>
                                    </select>
                                </div>

                                    @if ($errors->has('predecessorgoal'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('predecessorgoal') }}</strong>
                                    </span>
                                    @endif
                            </div>

                            <div class="form-group{{ $errors->has('publicstatistics') ? ' has-error' : '' }}">
                                <label for="public_statistics" class="col-md-4 control-label">Estadisticas</label>

                                <div class="col-md-6">
                                    <select name="publicstatistics" id="publicstatistics">
                                        <option value="N">Privadas</option>
                                        <option value="Y">Publicas</option>
                                    </select>
                                </div>

                                    @if ($errors->has('publicstatistics'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('publicstatistics') }}</strong>
                                    </span>
                                    @endif
                            </div>


                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Crear
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
