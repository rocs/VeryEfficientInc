@extends('layouts.app')
@section('head')
<?php
  $name = $cosas[0];
  $datos = $cosas[1];
  $estimado = $cosas[2];
 ?>
<script type="text/javascript">
window.onload = function () {
    var chart = new CanvasJS.Chart("chartContainer", {
      title:{
        text: "{{$name}}"
      },
      data: [
      {
        type: "spline",
        lineThickness: 1,
        dataPoints: [
          <?php foreach ($datos as $entry): ?>
            { x: new Date({{$entry['fecha']}}), y: {{$entry['amount']}} },
          <?php endforeach; ?>
        ]
      },{
        type: "spline",
        lineThickness: 4,
        dataPoints: [
          <?php foreach ($datos as $entry): ?>
            { x: new Date({{$entry['fecha']}}), y: {{$estimado}} },
          <?php endforeach; ?>
        ]
      }
      ]
    });
chart.render();
}
</script>
@endsection
@section('content')
  <div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Metas</div>

                <div class="panel-body">
                  <div id="chartContainer" style="height: 300px; width: 100%;"></div>
                </div>
            </div>
        </div>
    </div>
  </div>
@endsection
