@extends('layouts.app')
@section('head')
<?php
  $lags = $datos[0];
  $name = $datos[1];
 ?>
<script type="text/javascript">
window.onload = function () {
   {{$r =0}}
  var chart = new CanvasJS.Chart("chartContainer",
    {
      title:{
        text: "{{$name->short_description}}"
      },
      data: [
      {
        dataPoints: [
          <?php foreach ($lags as $entry): ?>
          <?php  $r += 10?>
          <?php $dato = $entry['weight_value']* $entry['persentage']/100 ?>
            { x: {{$r}}, y: {{$dato}}, label: '{{$entry['short_description']}}'},
          <?php endforeach; ?>
        ]
      }
      ]
    });

	chart.render();
}
</script>
@endsection
@section('content')
  <div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Metas</div>

                <div class="panel-body">
                  <div id="chartContainer" style="height: 300px; width: 100%;"></div>
                </div>
            </div>
        </div>
    </div>
  </div>
@endsection
