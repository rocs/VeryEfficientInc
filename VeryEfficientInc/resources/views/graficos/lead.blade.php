@extends('layouts.app')
@section('head')
<?php
  $lead = $datos[0];
  $sum = $datos[1];
  $stimate = $datos[2];
  $name = $datos[3];
 ?>
<script type="text/javascript">
window.onload = function () {
  var chart = new CanvasJS.Chart("chartContainer",
   {
     title:{
       <?php if ($lead->group_measure == 'Y'): ?>
         text: "{{$lead->short_description}} grupal"
       <?php else: ?>
         text: "{{$lead->short_description}} individual"
       <?php endif; ?>

     },
     data: [
     {
       dataPoints: [
       { y: {{$sum}}, label: "{{$name}}"},
       { y: {{$stimate}}, label: "Valor Estimado"}
       ]
     }
     ]
   });

	chart.render();
}
</script>
@endsection
@section('content')
  <div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Metas</div>

                <div class="panel-body">
                  <div id="chartContainer" style="height: 300px; width: 100%;"></div>
                </div>
            </div>
        </div>
    </div>
  </div>
@endsection
