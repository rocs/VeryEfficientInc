@extends('layouts.app')
@section('head')
<?php
//dd($datos);
$lead = $datos[0];
$stimate = $datos[1];
$name = $datos[2];
$amount = $datos[3];
 ?>
<script type="text/javascript">
window.onload = function () {
   {{$r =0}}
  var chart = new CanvasJS.Chart("chartContainer",
    {
      title:{
        text: "{{$name}}"
      },
      data: [
      {
        dataPoints: [

          <?php foreach ($amount as $entry): ?>

          <?php  $r += 10?>
            { x: {{$r}}, y: {{$entry['sum']}}, label: '{{$entry['name']}}'},
          <?php endforeach; ?>
          <?php  $r += 10?>
          { x: {{$r}}, y: {{$stimate}}, label: 'Valor Estimado'},
        ]
      }
      ]
    });

	chart.render();
}
</script>
@endsection
@section('content')
  <div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Metas</div>

                <div class="panel-body">
                  <div id="chartContainer" style="height: 300px; width: 100%;"></div>
                </div>
            </div>
        </div>
    </div>
  </div>
@endsection
