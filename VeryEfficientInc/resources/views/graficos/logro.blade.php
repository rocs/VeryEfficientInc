@extends('layouts.app')
@section('head')
<?php
  $amount = $datos[0];
  $name = $datos[1];
 ?>
<script type="text/javascript">
window.onload = function () {
  var chart = new CanvasJS.Chart("chartContainer",
  {
    title:{
      text: "{{$name}}"
    },
    data: [
      {
        type: "bar",
        dataPoints: [
          <?php foreach ($amount as $entry): ?>
            {y: {{$entry['estimado']}}, label: 'Estimado'},
          <?php endforeach; ?>
        ]
      },
    {
      type: "bar",
      dataPoints: [
        <?php foreach ($amount as $entry): ?>
          {y: {{$entry['amount']}}, label: '{{$entry['name']}}'},
        <?php endforeach; ?>
      ]
    }
    ]
  });

chart.render();
}
</script>
@endsection
@section('content')
  <div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Metas</div>

                <div class="panel-body">
                  <div id="chartContainer" style="height: 300px; width: 100%;"></div>
                </div>
            </div>
        </div>
    </div>
  </div>
@endsection
