@extends('layouts.app')
@section('head')
<?php
  $sum = $datos[0];
  $res = $datos[1];
  $name = $datos[2];
 ?>
<script type="text/javascript">
window.onload = function () {
	var chart = new CanvasJS.Chart("chartContainer",
	{
		title:{
			text: "{{$name->short_description}}"
		},
		legend: {
			maxWidth: 350,
			itemWidth: 120
		},
		data: [
		{
			type: "doughnut",
			showInLegend: true,
			legendText: "{indexLabel}",
			dataPoints: [
				{ y: {{$sum}}, indexLabel: "Completo " },
				{ y: {{$res}}, indexLabel: "Pendiente" }
			]
		}
		]
	});
	chart.render();
}
</script>
@endsection
@section('content')
  <div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Metas</div>

                <div class="panel-body">
                  <div id="chartContainer" style="height: 300px; width: 100%;"></div>
                </div>
            </div>
        </div>
    </div>
  </div>
@endsection
