@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Editar Goal</div>
                    <div class="panel-body">
                        <?php $lag = $cosas[0] ?>
                        <?php $goal = $cosas[1] ?>



                        <form class="form-horizontal" role="form" method="POST" action="{{ url('lag/editar', $lag->id) }}">
                            {{ csrf_field() }}
                            <input type="hidden" name="_method" value="PATCH">
                            <!-- Form Input descricion corta-->
                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label">Short Description</label>

                                <div class="col-md-6">
                                    <input id="shortdescription" type="text" class="form-control" name="shortdescription" value="{{ $lag->short_description }}" required autofocus>

                                    @if ($errors->has('shortdescription'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('shortdescription') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <!-- Form Input descricion larga-->
                            <div class="form-group{{ $errors->has('longdescription') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-4 control-label">Long Description</label>

                                <div class="col-md-6">
                                    <textarea id="longdescription" name="longdescription" type="text" class="form-control" value="{{ $lag->long_description }}" required autofocus>{{ $lag->long_description }}</textarea>
                                    @if ($errors->has('longdescription'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('longdescription') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <!-- Form Input Estado-->
                            <div class="form-group{{ $errors->has('weight_value') ? ' has-error' : '' }}">
                                <label for="weight_value" class="col-md-4 control-label">Weight Value</label>

                                <div class="col-md-6">
                                    <input id="weight_value" type="number" class="form-control" name="weight_value" value="{{ $lag->weight_value }}" required autofocus>
                                </div>

                                @if ($errors->has('weight_value'))
                                    <span class="help-block">
                                                <strong>{{ $errors->first('weight_value') }}</strong>
                                            </span>
                                @endif
                            </div>

                            <!-- Form Input equipo-->
                            <div class="form-group{{ $errors->has('goal_id') ? ' has-error' : '' }}">
                                <label for="goal_id" class="col-md-4 control-label">Goal</label>

                                <div class="col-md-6">
                                    <input readonly type="text" class="form-control" value="{{ $goal->short_description }}" required autofocus>
                                    <textarea rows="1" cols="2" id="goal_id" type="text" class="form-control" name="goal_id" value="{{$goal->id}}" style="visibility:hidden" required autofocus>{{$goal->id}}</textarea>
                                </div>

                                @if ($errors->has('goal_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('goal_id') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('persentage') ? ' has-error' : '' }}">
                                <label for="goal_id" class="col-md-4 control-label">Persentage</label>
                                <div class="col-md-6">
                                    <input type="number" id="persentage" name="persentage" class="form-control" value="{{ $lag->persentage }}" required autofocus>
                                </div>

                                @if ($errors->has('persentage'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('persentage') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <!-- Form Input Button-->
                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Actualizar
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
