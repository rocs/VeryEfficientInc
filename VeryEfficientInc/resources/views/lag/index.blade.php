@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Lag Measures</div>

                <div class="panel-body">
                  <?php
                  $goals = $cosas[0];
                  $lagMeasures = $cosas[1];
                  $id = $cosas[2];
                   ?>
                  <a href="{{ url('/goals/'.$id) }}"><button type="button" class="btn btn-success" name="button">Atras</button></a>
                  <br>
                  @if(count($lagMeasures)>0 && count($goals)>0)
                  <div class="table-responsive">
                  <table class="table table-bordered table-hover">
                      <thead>
                        <tr>
                          <td>
                            Short descriptionbre
                          </td>
                          <td>
                            Long description
                          </td>
                          <td>
                            	Weight value
                          </td>
                          <td>
                            Persentage
                          </td>
                          <td>
                            	Goal
                          </td>
                          <td>
                            Lead Measures
                          </td>
                          <td>
                            Accion
                          </td>
                        </tr>
                      </thead>

                      @foreach ($lagMeasures as $lagMeasure)
                      <tbody>
                        <td>{{$lagMeasure->short_description}}</td>
                        <td>{{$lagMeasure->long_description}}</td>
                        <td>{{$lagMeasure->weight_value}}</td>
                        <td>{{$lagMeasure->persentage}}</td>

                        <?php foreach ($goals as $goal): ?>
                          <?php if ($goal->id == $lagMeasure->goal_id): ?>
                            <td>{{$goal->short_description}}</td>
                          <?php endif; ?>
                        <?php endforeach; ?>
                        <td>
                          <a class="btn btn-primary" href="/lead/{!! $lagMeasure->id !!}/{!! $id !!}">Ver Lead Measures</a>
                            <a class="btn btn-primary" href="/lead/add/{!! $lagMeasure->id !!}/{!!$goal->id!!}">Añadir Lead Measures</a>
                        </td>
                        <td>
                          <a class="btn btn-primary" href="/lag/edit/{!! $lagMeasure->id !!}/{!! $id !!}">Editar</a>
                            <form action="{{ url('lag/destroy', $lagMeasure->id) }}" method="post">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="_method" value="DELETE">
                            <button type="submit" class="btn btn-danger">
                              <span  aria-hidden="true">Eliminar</span>
                            </button>
                          </form>
                        </td>
                      </tbody>
                       @endforeach
                    </table>
                      @else
                          <h2>No hay Lag Measures</h2>
                      @endif
                </div>
              </div>
            </div>
        </div>
    </div>
</div>
@endsection
