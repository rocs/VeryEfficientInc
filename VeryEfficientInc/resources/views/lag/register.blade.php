@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Editar Goal</div>
                    <div class="panel-body">
                        <?php $goals = $goals[0];?>
                        <form class="form-horizontal" role="form" method="POST" action="{{ url('/lag/register')}}">
                            {{ csrf_field() }}

                            <!-- Form Input descricion corta-->

                            <div class="form-group{{ $errors->has('shortdescription') ? ' has-error' : '' }}">
                                <label for="shortdescription" class="col-md-4 control-label">Short description</label>

                                <div class="col-md-6">
                                    <input id="shortdescription" type="text" class="form-control" name="shortdescription" value="{{ old('shortdescription') }}" required autofocus>

                                    @if ($errors->has('shortdescription'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('shortdescription') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <!-- Form Input descricion larga-->
                                <div class="form-group{{ $errors->has('longdescription') ? ' has-error' : '' }}">
                                    <label for="email" class="col-md-4 control-label">Long Description</label>

                                    <div class="col-md-6">
                                        <textarea id="longdescription" name="longdescription" type="text" class="form-control" value="{{ old('longdescription') }}" required autofocus></textarea>
                                        @if ($errors->has('longdescription'))
                                            <span class="help-block">
                                            <strong>{{ $errors->first('longdescription') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>

                                <!-- Form Input Estado-->
                                    <div class="form-group{{ $errors->has('weight_value') ? ' has-error' : '' }}">
                                        <label for="weight_value" class="col-md-4 control-label">Weight Value</label>

                                        <div class="col-md-6">
                                          <input id="weight_value" type="number" class="form-control" name="weight_value" value="{{ old('weight_value') }}" required autofocus>
                                        </div>

                                        @if ($errors->has('weight_value'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('weight_value') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                            <!-- Form Input equipo-->
                            <div class="form-group{{ $errors->has('goal_id') ? ' has-error' : '' }}">
                                <label for="goal_id" class="col-md-4 control-label">Goal</label>

                                <div class="col-md-6">
                                  <input readonly type="text" class="form-control" value="{{ $goals->short_description }}" required autofocus>
                                  <textarea rows="1" cols="2" id="goal_id" type="text" class="form-control" name="goal_id" value="{{$goals->id}}" style="visibility:hidden" required autofocus>{{$goals->id}}</textarea>
                                </div>

                                @if ($errors->has('goal_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('goal_id') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('persentage') ? ' has-error' : '' }}">
                                <label for="goal_id" class="col-md-4 control-label">Persentage</label>
                                <?php $goals = $goals[0];?>
                                <div class="col-md-6">
                                    <input type="number" id="persentage" name="persentage" class="form-control" value="{{ old('persentage') }}" required autofocus>
                                </div>

                                @if ($errors->has('persentage'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('persentage') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <!-- Form Input Button-->
                                <div class="form-group">
                                    <div class="col-md-6 col-md-offset-4">
                                        <button type="submit" class="btn btn-primary">
                                            Crear
                                        </button>
                                    </div>
                                </div>
                            </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
