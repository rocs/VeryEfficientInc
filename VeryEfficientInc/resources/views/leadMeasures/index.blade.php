@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12 ">
                <div class="panel panel-default">
                    <div class="panel-heading">Lead Measures</div>

                    <div class="panel-body">
                      <?php
                      $leads = $cosas[0];
                      $lag = $cosas[1];
                      //dd($lag);
                      $lag = $lag[0];
                      $grup = $cosas[2];
                      ?>
                        <a href="{{ url('/lag/'.$grup) }}"><button type="button" class="btn btn-success" name="button">Atras</button></a>
                        <br>
                        @if(count($leads)>0)
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <td>
                                            Short Description
                                        </td>
                                        <td>
                                            Long Description
                                        </td>
                                        <td>
                                            Lag Measure
                                        </td>
                                        <td>
                                            Stimate Value
                                        </td>
                                        <td>
                                            Periodicity Days
                                        </td>
                                        <td>
                                            Group Measure
                                        </td>
                                        <td>
                                            Start Date
                                        </td>
                                        <td>
                                            Estatus
                                        </td>
                                        <td>
                                          Lead Measure Logs
                                        </td>
                                        <td>
                                          Graficos
                                        </td>
                                        <td>
                                            Accion
                                        </td>
                                    </tr>
                                    </thead>
                                    @foreach ($leads as $lead)
                                        <tbody>
                                        <td>{{$lead->short_description}}</td>
                                        <td>{{$lead->long_description}}</td>
                                        <td>{{$lag->short_description}}</td>
                                        <td>{{$lead->stimate_value}}</td>
                                        <td>{{$lead->periodicity_days}}</td>
                                        <?php if ($lead->group_measure === 'Y'): ?>
                                          <td>Yes</td>
                                        <?php else: ?>
                                          <td>No</td>
                                        <?php endif; ?>
                                        <td>{{$lead->start_date}}</td>
                                        <?php if ($lead->estatus === 'E'): ?>
                                          <td>{{$lead->estatus}}</td>
                                        <?php else: ?>
                                          <td>{{$lead->estatus}}</td>
                                        <?php endif; ?>
                                        <td>
                                          <a class="btn btn-primary" href="/log">Ver Lead Measure Logs</a>
                                            <a class="btn btn-primary" href="/log/register/{!! $lead->id !!}">Crear Lead Measure Logs</a>
                                        </td>
                                        <td>
                                          <a class="btn btn-primary" href="/grafico/grupal/{!! $lead->id !!}/{!! $grup !!}">Ver Predictiva grupal</a>
                                          <a class="btn btn-primary" href="/grafico/usuario/{!! $lead->id !!}/{!! $grup !!}">Ver Predictiva induvidual</a>
                                          <a class="btn btn-primary" href="/grafico/fecha/{!! $lead->id !!}">Ver Estimado vs Logrado</a>
                                        </td>
                                        <td>
                                            <a class="btn btn-primary" href="/lead/edit/{!! $lead->id !!}/{!! $lag->id !!}">Editar</a>
                                            <form action="{{ url('/lead/destroy', $lead->id) }}" method="post">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                <input type="hidden" name="_method" value="DELETE">
                                                <button type="submit" class="btn btn-danger">
                                                    <span  aria-hidden="true">Eliminar</span>
                                                </button>
                                            </form>
                                        </td>
                                        </tbody>
                                    @endforeach
                                </table>
                                @else
                                    <h2>No hay Lead Measures</h2>
                                @endif
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
