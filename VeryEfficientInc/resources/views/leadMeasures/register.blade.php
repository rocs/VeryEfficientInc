@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Editar Lead Measures</div>
                    <div class="panel-body">
                        <?php
                        //dd($cosas);
                        $lag = $cosas[0];
                        $lag = $lag[0];
                        $grup = $cosas[1];
                        ?>
                        <form class="form-horizontal" role="form" method="POST" action="{{ url('/lead/register/'.$grup)}}">
                            {{ csrf_field() }}

                            <!-- Form Input descricion corta-->

                            <div class="form-group{{ $errors->has('shortdescription') ? ' has-error' : '' }}">
                                <label for="shortdescription" class="col-md-4 control-label">Short description</label>

                                <div class="col-md-6">
                                    <input id="shortdescription" type="text" class="form-control" name="shortdescription" value="{{ old('shortdescription') }}" required autofocus>

                                    @if ($errors->has('shortdescription'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('shortdescription') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <!-- Form Input descricion larga-->
                                <div class="form-group{{ $errors->has('longdescription') ? ' has-error' : '' }}">
                                    <label for="email" class="col-md-4 control-label">Long Description</label>

                                    <div class="col-md-6">
                                        <textarea id="longdescription" name="longdescription" type="text" class="form-control" value="{{ old('longdescription') }}" required autofocus></textarea>
                                        @if ($errors->has('longdescription'))
                                            <span class="help-block">
                                            <strong>{{ $errors->first('longdescription') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('	lag') ? ' has-error' : '' }}">
                                    <label for="	lag_measure_id" class="col-md-4 control-label">Lag Measure</label>

                                    <div class="col-md-6">
                                      <input readonly type="text" class="form-control" value="{{ $lag->short_description }}" required autofocus>
                                      <textarea readonly rows="1" cols="2" id="lag" type="text" class="form-control" name="lag" value="{{$lag->id}}" style="visibility:hidden" required autofocus>{{$lag->id}}</textarea>
                                    </div>

                                    @if ($errors->has('lag'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('lag') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <!-- Form Input Estado-->
                                    <div class="form-group{{ $errors->has('stimate_value') ? ' has-error' : '' }}">
                                        <label for="weight_value" class="col-md-4 control-label">Stimate Value</label>

                                        <div class="col-md-6">
                                          <input id="stimate_value" type="number" class="form-control" name="stimate_value" value="{{ old('stimate_value') }}" required autofocus>
                                        </div>

                                        @if ($errors->has('stimate_value'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('stimate_value') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                            <!-- Form Input equipo-->


                            <div class="form-group{{ $errors->has('periodicity_days') ? ' has-error' : '' }}">
                                <label for="periodicity_days" class="col-md-4 control-label">Periodicity Days</label>

                                <div class="col-md-6">
                                    <input type="number" id="periodicity_days" name="periodicity_days" class="form-control" value="{{ old('periodicity_days') }}" required autofocus>
                                </div>

                                @if ($errors->has('periodicity_days'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('periodicity_days') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('group_measure') ? ' has-error' : '' }}">
                                <label for="public_statistics" class="col-md-4 control-label">Group Measure</label>

                                <div class="col-md-6">
                                    <select name="group_measure" id="group_measure">
                                        <option value="Y">Yes</option>
                                        <option value="N">No</option>
                                    </select>
                                </div>

                                    @if ($errors->has('group_measure'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('group_measure') }}</strong>
                                    </span>
                                    @endif
                            </div>

                            <div class="form-group{{ $errors->has('start_date') ? ' has-error' : '' }}">
                                <label for="start_date" class="col-md-4 control-label">Start Date</label>
                                <div class="col-md-6">
                                    <input type="date" id="start_date" name="start_date" class="form-control" value="{{ old('start_date') }}" required autofocus>
                                </div>

                                @if ($errors->has('start_date'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('start_date') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('estatus') ? ' has-error' : '' }}">
                                <label for="public_statistics" class="col-md-4 control-label">Estatus</label>

                                <div class="col-md-6">
                                    <select name="estatus" id="estatus">
                                        <option value="E">E</option>
                                        <option value="D">D</option>
                                    </select>
                                </div>

                                    @if ($errors->has('estatus'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('estatus') }}</strong>
                                    </span>
                                    @endif
                            </div>

                            <!-- Form Input Button-->
                                <div class="form-group">
                                    <div class="col-md-6 col-md-offset-4">
                                        <button type="submit" class="btn btn-primary">
                                            Crear
                                        </button>
                                    </div>
                                </div>
                            </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
