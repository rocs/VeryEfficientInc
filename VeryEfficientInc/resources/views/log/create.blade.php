@extends('layouts.app')

@section('content')
@parent
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                  <?php
                  $usuario = $datos[0];
                  $lead = $datos[1];
                   ?>
                    <div class="panel-heading">Crear Lead Measure Log</div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="{{ url('/log/registro')}}">
                          {{ csrf_field() }}
                          <div class="form-group">
                                 <label for="user_id" class="col-md-4 control-label">Usuario</label>
                                 <div class="col-md-6">
                                   <input type="" name="name" value="{{$usuario->name}}" readonly>
                                   <input type="hidden" name="user_id" value="{{$usuario->id}}">
                                 </div>
                             </div>
                             <div class="form-group">
                                    <label for="user_id" class="col-md-4 control-label">Lead Measure</label>
                                    <div class="col-md-6">
                                      <input type="" name="" value="{{$lead->short_description}}" readonly>
                                      <input type="hidden" name="lead_id" value="{{$lead->id}}">
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('amount') ? ' has-error' : '' }}">
                                    <label for="weight_value" class="col-md-4 control-label">Amount</label>

                                    <div class="col-md-6">
                                      <input id="weight_value" type="number" class="form-control" name="amount" value="{{ old('amount') }}" required autofocus>
                                    </div>

                                    @if ($errors->has('amount'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('amount') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group{{ $errors->has('log_time') ? ' has-error' : '' }}">
                                    <label for="periodicity_days" class="col-md-4 control-label">Log Time</label>

                                    <div class="col-md-6">
                                          <input type="date" id="log_time" name="log_time" class="form-control" value="{{ old('log_time') }}" required autofocus>
                                    </div>

                                    @if ($errors->has('log_time'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('log_time') }}</strong>
                                        </span>
                                    @endif
                                </div>
                             <div class="form-group">
                                 <div class="col-md-6 col-md-offset-4">
                                     <button type="submit" class="btn btn-primary">
                                          Crear
                                     </button>
                                 </div>
                             </div>
                       </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @stop
