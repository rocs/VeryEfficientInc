@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Lead Measures logs</div>
                    <div class="panel-body">
                        <a href="#"><button type="button" class="btn btn-success" name="button">Atras</button></a>
                        <br>
                        @if(count($items)>0)
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <td>
                                            Lead Measure
                                        </td>
                                        <td>
                                            Usuario
                                        </td>
                                        <td>
                                            Amount
                                        </td>
                                        <td>
                                            Log_time
                                        </td>
                                        <td>
                                            Accion
                                        </td>
                                    </tr>
                                    </thead>
                                    @foreach ($items as $log)
                                        <tbody>
                                        <td>{{$log->short_description}}</td>
                                        <td>{{$log->name}}</td>
                                        <td>{{$log->amount}}</td>
                                        <td>{{$log->log_time}}</td>
                                        <td>
                                            <a class="btn btn-primary" href="/log/edit/{!! $log->id !!}">Editar</a>
                                            <form action="{{ url('/log/destroy', $log->id) }}" method="post">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                <input type="hidden" name="_method" value="DELETE">
                                                <button type="submit" class="btn btn-danger">
                                                    <span  aria-hidden="true">Eliminar</span>
                                                </button>
                                            </form>
                                        </td>
                                        </tbody>
                                    @endforeach
                                </table>
                                @else
                                    <h2>No hay Lead Measures log</h2>
                                @endif
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
