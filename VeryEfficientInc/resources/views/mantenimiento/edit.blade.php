@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Editar usuario</div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="{{ url('/user/edit', $usuario->id) }}">
                            {{ csrf_field() }}
                            <input type="hidden" name="_method" value="PATCH">
                            @include('mantenimiento.partials.form',['submitButtonText'=>'Actualizar'])
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
