@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Usuarios</div>

                    <div class="panel-body">

                        <a href="{{ url('/register') }}"><button type="button" class="btn btn-success" name="button">Nuevo Usuario</button></a>
                        <br>
                        @if(count($Usuario)>0)
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <td>
                                            Nombre
                                        </td>
                                        <td>
                                            Email
                                        </td>
                                        <td>
                                            Rol
                                        </td>
                                        <td>
                                            Accion
                                        </td>
                                    </tr>
                                    </thead>
                                    @foreach ($Usuario as $user)
                                        <tbody>
                                        <td>{{$user->name}}</td>
                                        <td>{{$user->email}}</td>
                                        @if($user->rol === 1)
                                            <td>Administrador</td>
                                        @else
                                            <td>Usuario</td>
                                        @endif
                                        <td>
                                            <a class="btn btn-primary" href="/edit/{!! $user->id !!}">Editar</a>
                                            <form action="{{ url('/destroy', $user->id) }}" method="post">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                <input type="hidden" name="_method" value="DELETE">
                                                <button type="submit" class="btn btn-danger">
                                                    <span  aria-hidden="true">Eliminar</span>
                                                </button>
                                            </form>
                                        </td>
                                        </tbody>
                                    @endforeach
                                </table>
                                @else
                                <h2>No hay Usuarios</h2>
                                @endif
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
