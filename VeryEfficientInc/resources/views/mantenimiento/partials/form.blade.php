
<!-- Form Input Name-->
    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
        <label for="name" class="col-md-4 control-label">Name</label>

        <div class="col-md-6">
            <input id="name" type="text" class="form-control" name="name" value="{{ isset($usuario->name) ? $usuario->name : '' }}" required autofocus>

            @if ($errors->has('name'))
                <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
            @endif
        </div>
    </div>

<!-- Form Input Email-->
    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
        <label for="email" class="col-md-4 control-label">Email</label>

        <div class="col-md-6">
            <input id="email" name="email" type="email" class="form-control" value="{{ isset($usuario->email) ? $usuario->email : '' }}" required autofocus>
            @if ($errors->has('email'))
                <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
            @endif
        </div>
    </div>

<!-- Form Input rol-->
    <div class="form-group{{ $errors->has('rol') ? ' has-error' : '' }}">
        <label for="rol" class="col-md-4 control-label">Rol</label>

        <div class="col-md-6">
            <select name="rol" id="rol">
                @if($usuario->rol === 1)
                    <option value="1">Administrador</option>
                    <option value="0">Usuario</option>
                @else
                    <option value="0">Usuario</option>
                    <option value="1">Administrador</option>
                @endif
            </select>
        </div>

        @if ($errors->has('rol'))
            <span class="help-block">
                <strong>{{ $errors->first('rol') }}</strong>
            </span>
        @endif
    </div>

<!-- Form Input Button-->
    <div class="form-group">
        <div class="col-md-6 col-md-offset-4">
            <button type="submit" class="btn btn-primary">
                {{ $submitButtonText }}
            </button>
        </div>
    </div>
</form>
