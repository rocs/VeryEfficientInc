@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Equipos</div>
                <div class="panel-body">
                  <a href="{{ url('team/register') }}"><button type="button" class="btn btn-success" name="button">Crear equipos</button></a>
                  <br>
                  @if(count($Teams)>0)
                  <div class="table-responsive">
                  <table class="table table-bordered table-hover">
                      <thead>
                        <tr>
                          <td>
                            Nombre
                          </td>
                          <td>
                            Descripcion
                          </td>
                          <td>
                            Accion
                          </td>
                        </tr>
                      </thead>
                      @foreach ($Teams as $team)
                      <tbody>
                        <td>{{$team->team_id}}</td>
                        <td>{{$team->description}}</td>
                        <td>
                            <a class="btn btn-primary" href="team/edit/{!! $team->id !!}">Editar</a>
                            <a class="btn btn-primary" href="goals/{!! $team->id !!}">Ver Goals</a>
                            <a class="btn btn-primary" href="goals/add/{!! $team->id !!}">Añadir Goals</a>
                            <a class="btn btn-primary" href="teamUser/membresia/{!! $team->id !!}/{!! $team->team_id !!}">Membresias</a>
                            <form action="{{ url('team/destroy', $team->id) }}" method="post">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="_method" value="DELETE">
                            <button type="submit" class="btn btn-danger">
                              <span  aria-hidden="true">Eliminar</span>
                            </button>
                          </form>
                        </td>
                      </tbody>
                       @endforeach
                    </table>
                      @else
                          <h2>No hay Equipos</h2>
                      @endif
                </div>
              </div>
            </div>
        </div>
    </div>
</div>
@endsection
