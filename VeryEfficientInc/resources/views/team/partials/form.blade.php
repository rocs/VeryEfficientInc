
<!-- Form Input Team_id-->
    <div class="form-group{{ $errors->has('team_id') ? ' has-error' : '' }}">
        <label for="name" class="col-md-4 control-label">Name</label>

        <div class="col-md-6">
            <input id="team_id" type="text" class="form-control" name="team_id" value="{{ isset($team->team_id) ? $team->team_id : '' }}" required autofocus>

            @if ($errors->has('team_id'))
                <span class="help-block">
                <strong>{{ $errors->first('team_id') }}</strong>
            </span>
            @endif
        </div>
    </div>

<!-- Form Input Description-->
    <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
        <label for="email" class="col-md-4 control-label">Descripcion</label>

        <div class="col-md-6">
            <input id="description" name="description" class="form-control" value="{{ isset($team->description) ? $team->description : '' }}" required autofocus>
            @if ($errors->has('description'))
                <span class="help-block">
                <strong>{{ $errors->first('description') }}</strong>
            </span>
            @endif
        </div>
    </div>

<!-- Form Input Button-->
    <div class="form-group">
        <div class="col-md-6 col-md-offset-4">
            <button type="submit" class="btn btn-primary">
                {{ $submitButtonText }}
            </button>
        </div>
    </div>
</form>
