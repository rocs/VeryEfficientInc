@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Usuarios por Equipos</div>

                <div class="panel-body">

                  <a href="{{ url('teamUser/register') }}"><button type="button" class="btn btn-success" name="button">Unir usuario</button></a>

                  <br>
                  <div class="table-responsive">
                  <table class="table table-bordered table-hover">
                      <thead>
                        <tr>
                          <td>
                            Equipos
                          </td>
                          <td>
                            Usuarios
                          </td>
                          <td>
                            Rol
                          </td>
                          <td>
                            Accion
                          </td>
                        </tr>
                      </thead>
                        @forelse ($items as $teamUser)
                      <tbody>
                          <td>{{$teamUser->team_id}}</td>
                          <td>{{$teamUser->name}}</td>
                          <td>
                          <?php if ($teamUser->rol === 'A'): ?>
                            Administrador
                          <?php elseif($teamUser->rol === 'A'): ?>
                            Lectura/Escritura
                          <?php else:?>
                            Lectura
                          <?php endif; ?>
                        </td>
                          <td>
                            <a class="btn btn-primary" href="teamUser/edit/{!! $teamUser->id !!}">Editar</a>
                            <form action="{{ url('teamUser/destroy', $teamUser->id) }}" method="post">
                              <input type="hidden" name="_token" value="{{ csrf_token() }}">
                              <input type="hidden" name="_method" value="DELETE">
                              <button type="submit" class="btn btn-danger">
                                <span  aria-hidden="true">Eliminar</span>
                              </button>
                            </form>
                          </td>
                      @empty
                        <h2>No hay datos</h2>
                      @endforelse
                      </tbody>
                    </table>
                </div>
              </div>
            </div>
        </div>
    </div>
</div>
@endsection
