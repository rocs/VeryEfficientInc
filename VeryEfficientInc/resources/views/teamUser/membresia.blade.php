@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Mantenimiento de membresias de usuario</div>
                <?php
                $teamUsers = $datos[0];
                $usuarios = $datos[1];
                $teamName =$datos[2];
                $idTeam = $datos[3];
                //dd($idTeam);
                 ?>
                <div class="panel-body">
                  <div class="row text-center">
                  <div class="form-group">
                         <label for="user_id" class="col-md-4 control-label">Equipo</label>
                         <div class="col-md-5">
                           <input type="text" name="name" value="{{$teamName}}" readonly>
                        </div>
                         <div class="col-md-3">
                           <a href="/team"><button type="button" class="btn btn-success" name="button">Atras</button></a>
                         </div>
                     </div>
                   </div>
                   <div class="row text-center" style="border: 1px solid #abbec9">
                      <div class="col-md-4" >
                        <label for="">Administrador</label>
                        <ul class="list-group">
                          @foreach ($teamUsers as $teamUser)
                              @if ($teamUser->rol[0] === 'A')
                                @foreach ($usuarios as $usuario)
                                  <?php if (''.$usuario->id === $teamUser->user_id): ?>
                                      <div class="list-group-item">
                                    <p value="" >{{$usuario->name}}</p>
                                  <?php endif; ?>
                                @endforeach
                                <a href="/teamUser/cambio/{!!$teamUser->id!!}/{!!$teamName!!}/{{!!$idTeam!!}}"> <img src="/iconos/flecha_derecha.jpg" alt="" /></a>
                                </div>
                              @endif
                          @endforeach
                        </ul>
                      </div>
                      <div class="col-md-4" >
                         <label for="">Membresia</label>
                        <ul class="list-group">
                            @foreach ($teamUsers as $teamUser)

                                @if ($teamUser->rol[0] === 'R' || $teamUser->rol[0] === 'W')
                                  @foreach ($usuarios as $usuario)
                                  <?php if (''.$usuario->id === $teamUser->user_id): ?>
                                      <div class="list-group-item">
                                    <p value="" >{{$usuario->name}}</p>
                                  <?php endif; ?>
                                  @endforeach
                                <a href="/teamUser/cambio/{!!$teamUser->id!!}/{!!$teamName!!}/{!!$idTeam!!}"> <img src="/iconos/flecha_izquierda.jpg" alt="" /></a>
                                <form action="{{ url('teamUser/destroyE', $teamUser->id.'/'.$teamName.'/'.$idTeam) }}" method="post">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" name="_method" value="DELETE">
                                <input type='image' name='submit' value='Submit' src="/iconos/flecha_derecha.jpg" alt="Submit">
                              </form>
                              </div>
                                @endif
                            @endforeach
                        </ul>
                      </div>
                      <div class="col-md-4">
                        <label for="">Usuario</label>
                        <ul class="list-group">
                            @forelse ($teamUsers as $teamUser)
                            <?php $valor = true ?>
                                <?php foreach ($usuarios as $usuario): ?>
                                      <?php if ($usuario->id != $teamUser->user_id): ?>
                                        {{$valor = false}}
                                      <?php endif; ?>
                                <?php endforeach; ?>
                                <?php if ($valor): ?>
                                  <div class="list-group-item">
                                    <p>{{$usuario['name']}}</p>
                                    <form class="form-horizontal" role="form" method="POST" action="/teamUser/unir">
                                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                      <input type="hidden" name="team_id" value="{{$idTeam}}">
                                      <input type="hidden" name="user_id" value="{{$usuario->id}}">
                                      <input type="hidden" name="idTeam" value="{{$idTeam}}">
                                      <input type="hidden" name="teamName" value="{{$teamName}}">
                                      <input type='image' name='submit' value='Submit' src="/iconos/flecha_izquierda.jpg" alt="Submit">
                                    </form>
                                  </div>
                                <?php endif; ?>
                            @empty
                            <p>
                              No hay usuarios
                            </p>
                            @endforelse
                        </ul>
                      </div>
                   </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
