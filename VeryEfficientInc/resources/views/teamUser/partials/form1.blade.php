
<!-- Form Input Team_id-->
    <div class="form-group{{ $errors->has('user_id') ? ' has-error' : '' }}">
        <label for="user_id" class="col-md-4 control-label">Usuario</label>

        <div class="col-md-6">
            <select class="form-control" name="user_id">
              @foreach ($usuarios as $usuario)
              <option value="{{$usuario->id}}">{{$usuario->name}}</option>
              @endforeach
            </select>

            @if ($errors->has('user_id'))
                <span class="help-block">
                <strong>{{ $errors->first('user_id') }}</strong>
            </span>
            @endif
        </div>
    </div>

<!-- Form Input Description-->
   <div class="form-group{{ $errors->has('team_id') ? ' has-error' : '' }}">
        <label for="team_id" class="col-md-4 control-label">Equipo</label>

        <div class="col-md-6">
          <select class="form-control" name="team_id">
            @foreach ($teams as $team)
              <option value="{{$team->id}}">{{$team->team_id}}</option>
            @endforeach
          </select>

            @if ($errors->has('team_id'))
                <span class="help-block">
                <strong>{{ $errors->first('team_id') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group{{ $errors->has('rol') ? ' has-error' : '' }}">
         <label for="rol" class="col-md-4 control-label">Rol en el equipo</label>

         <div class="col-md-6">
           <select class="form-control" name="rol">
               <option value="R">Lectura</option>
               <option value="W">Escritura/Lectura</option>
               <option value="A">Administrador</option>
           </select>

             @if ($errors->has('rol'))
                 <span class="help-block">
                 <strong>{{ $errors->first('rol') }}</strong>
                 </span>
             @endif
         </div>
     </div>

<!-- Form Input Button-->
    <div class="form-group">
        <div class="col-md-6 col-md-offset-4">
            <button type="submit" class="btn btn-primary">
                {{ $submitButtonText }}
            </button>
        </div>
    </div>
</form>
