@extends('layouts.app')

@section('content')
@parent
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Usuario por equipo</div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="registro">
                          {{ csrf_field() }}
                          @include('teamUser.partials.form1',['submitButtonText'=>'Guardar'])
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @stop
