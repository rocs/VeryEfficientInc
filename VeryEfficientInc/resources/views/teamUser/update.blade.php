@extends('layouts.app')

@section('content')
@parent
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                  <?php
                  $teamUser = $datos[0];
                  $nameUser = $datos[1];
                  $teams = $datos[2];
                   ?>
                    <div class="panel-heading">Usuario por equipo</div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="{{ url('/teamUser/editar', $teamUser->id) }}">
                          {{ csrf_field() }}
                          <input type="hidden" name="_method" value="PATCH">
                          <div class="form-group">
                                 <label for="user_id" class="col-md-4 control-label">Usuario</label>
                                 <div class="col-md-6">
                                   <input type="" name="name" value="{{$nameUser->name}}" readonly>
                                   <input type="hidden" name="user_id" value="{{$nameUser->id}}">
                                 </div>
                             </div>
                             <div class="form-group{{ $errors->has('team_id') ? ' has-error' : '' }}">
                                  <label for="team_id" class="col-md-4 control-label">Equipo</label>
                                  <div class="col-md-6">
                                    <select class="form-control" name="team_id">
                                      @foreach ($teams as $team)
                                      @if (''.$team->id === $teamUser->team_id)
                                          <option value= "{{$team->id}}" >{{$team->team_id}}</option>
                                        @endif
                                      @endforeach
                                      @foreach ($teams as $team)
                                        <option value="{{$team->id}}">{{$team->team_id}}</option>
                                      @endforeach
                                    </select>
                                      @if ($errors->has('team_id'))
                                          <span class="help-block">
                                          <strong>{{ $errors->first('team_id') }}</strong>
                                          </span>
                                      @endif
                                  </div>
                              </div>
                             <div class="form-group{{ $errors->has('rol') ? ' has-error' : '' }}">
                                  <label for="rol" class="col-md-4 control-label">Rol en el equipo</label>
                                  <div class="col-md-6">
                                    <select class="form-control" name="rol">
                                      <?php if ($teamUser->rol === 'A'): ?>
                                          <option value="A">Administrador</option>
                                          <option value="R">Lectura</option>
                                          <option value="W">Escritura/Lectura</option>
                                      <?php elseif($teamUser->rol === 'W'): ?>
                                          <option value="W">Escritura/Lectura</option>
                                          <option value="R">Lectura</option>
                                          <option value="A">Administrador</option>
                                      <?php else:?>
                                        <option value="R">Lectura</option>
                                        <option value="A">Administrador</option>
                                        <option value="W">Escritura/Lectura</option>
                                      <?php endif; ?>
                                    </select>

                                      @if ($errors->has('rol'))
                                          <span class="help-block">
                                          <strong>{{ $errors->first('rol') }}</strong>
                                          </span>
                                      @endif
                                  </div>
                              </div>
                             <div class="form-group">
                                 <div class="col-md-6 col-md-offset-4">
                                     <button type="submit" class="btn btn-primary">
                                          Guardar
                                     </button>
                                 </div>
                             </div>
                       </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @stop
