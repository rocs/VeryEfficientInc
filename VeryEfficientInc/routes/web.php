<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    if (Auth::check()){
        return view('home');
    }else{
        return view('welcome');
    }
});


Auth::routes();

Route::get('/home', 'HomeController@index');

Route::get('/register', function () {
    if (Auth::user()->rol == 1){
        return view('registro/register');
    }else{
        return view('errors/403');
    }
});

Route::get('/maintenance', 'UserController@index');
Route::get('mantenimiento/maintenance', 'UserController@index');

Route::get('/goals/{id}', 'GoalController@index');
Route::get('goals/add/{id}', 'GoalController@create');
Route::post('goals/create', 'GoalController@store');
Route::delete('goal/destroy/{id}/{idTeam}', 'GoalController@destroy');
Route::get('goal/edit/{id}', 'GoalController@edit');
Route::match(['put', 'patch'], 'goals/edit/{id}', 'GoalController@update');

Route::get('/lag/{id}', 'lagMeasureController@index');
Route::get('/lag/add/{id}', 'lagMeasureController@create');
Route::post('/lag/register', 'lagMeasureController@store');
Route::get('/lag/edit/{id}/{id_goal}', 'lagMeasureController@edit');
Route::match(['put', 'patch'], 'lag/editar/{id}', 'lagMeasureController@update');
Route::delete('lag/destroy/{id}', 'lagMeasureController@destroy');

Route::get('/lead/{id}/{idGrup}', 'leadMeasuresController@index');
Route::get('/lead/add/{id}/{idGrup}', 'leadMeasuresController@create');
Route::post('/lead/register/{id}', 'leadMeasuresController@store');
Route::get('/lead/edit/{id}/{idLag}', 'leadMeasuresController@edit');
Route::match(['put', 'patch'], '/lead/update/{id}', 'leadMeasuresController@update');
Route::delete('/lead/destroy/{id}', 'leadMeasuresController@destroy');

Route::resource('registro', 'UserController', ['only'=> ['store','create']]);

//Route Team
Route::get('/team', 'TeamController@index');

Route::get('team/register', function()
{ if (Auth::user()->rol == 1){
    return view('team/register');
}else{
    return view('errors/403');
}

});

Route::get('/edit/{id}', 'UserController@edit');
Route::match(['put', 'patch'], 'user/edit/{id}', 'UserController@update');
Route::delete('/destroy/{id}', 'UserController@destroy');

Route::get('team/edit/{id}', 'TeamController@edit');
Route::post('team/registro', 'TeamController@store');
Route::match(['put', 'patch'], '/team/editar/{id}', 'TeamController@update');
Route::delete('team/destroy/{id}', 'TeamController@destroy');

//Route TeamUser
Route::get('/teamUser','TeamUserController@index');

Route::get('teamUser/register', 'TeamUserController@create');
Route::get('teamUser/edit/{id}', 'TeamUserController@edit');
Route::post('teamUser/registro', 'TeamUserController@store');
Route::match(['put', 'patch'], '/teamUser/editar/{id}', 'TeamUserController@update');
Route::delete('teamUser/destroy/{id}', 'TeamUserController@destroy');

//Route TeamUserEspecial
Route::get('teamUser/membresia/{id}/{teamName}', 'TeamUserController@especial');
Route::get('teamUser/cambio/{id}/{teamName}/{idTeam}', 'TeamUserController@cambio');
Route::delete('teamUser/destroyE/{id}/{teamName}/{idTeam}', 'TeamUserController@destroy1');
Route::post('teamUser/unir', 'TeamUserController@nuevo');

//Route Lead Measure
Route::get('/log','leadMeasureLogs@index');
Route::get('/log/register/{id}', 'leadMeasureLogs@create');
Route::post('/log/registro', 'leadMeasureLogs@store');
Route::get('/log/edit/{id}', 'leadMeasureLogs@edit');
Route::match(['put', 'patch'],'/log/update/{id}', 'leadMeasureLogs@update');
Route::delete('log/destroy/{id}', 'leadMeasureLogs@destroy');

//Route Graficos
Route::get('/grafico/meta/{id}','GraficosController@grafico1');
Route::get('/grafico/lag/{id}','GraficosController@grafico2');
Route::get('/grafico/grupal/{leadid}/{id}','GraficosController@grafico3');
Route::get('/grafico/usuario/{leadid}/{id}','GraficosController@grafico4');
Route::get('/grafico/logro/{id}','GraficosController@grafico5');
Route::get('/grafico/fecha/{id}','GraficosController@grafico6');
